<?php

declare(strict_types=1);

namespace Hewsda\FirewallJwt;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class JwtServiceProvider extends ServiceProvider
{
    /**
     * @var bool
     */
    protected $defer = true;

    public function boot(): void
    {
        $this->publishes([
            __DIR__.'/../config/firewall_jwt.php' => config_path('firewall_jwt.php'),
        ]);
    }

    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/firewall_jwt.php', 'firewall_jwt');

        $this->app->singleton(JwtServiceManager::class, function (Application $app) {
            return new JwtServiceManager($app, $app['config']->get('firewall_jwt'));
        });
    }

    public function provides(): array
    {
        return [JwtServiceManager::class];
    }
}