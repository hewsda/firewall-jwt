<?php

declare(strict_types=1);

namespace Hewsda\FirewallJwt;

use Hewsda\FirewallJwt\Exception\InvalidArgumentException;
use Hewsda\FirewallJwt\Preset\Configuration;
use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Foundation\Application;
use Lcobucci\JWT\Signer\Key;

class JwtServiceManager
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var array
     */
    private $config;

    /**
     * @var array
     */
    private $drivers = [];

    /**
     * @var string
     */
    private $namespace = 'jwt_scopes';

    /**
     * JwtServiceManager constructor.
     *
     * @param Container $container
     * @param array $config
     */
    public function __construct(Container $container, array $config)
    {
        $this->container = $container;
        $this->config = $config;
    }

    public function create(string $driver): JwtGuard
    {
        if (null === $this->fromConfig($driver)) {
            throw new InvalidArgumentException(sprintf('Scope %s does not exist', $driver));
        }

        if (!isset($this->drivers[$driver])) {
            $this->drivers[$driver] = $this->createDriver($driver);
        }

        return $this->drivers[$driver];
    }

    protected function createDriver(string $scope): JwtGuard
    {
        $this->registerEventListener($scope);

        return new JwtGuard(
            $this->resolvePreset($scope),
            $this->resolveKey($scope),
            $scope,
            $this->container->make('events')
        );
    }

    protected function resolvePreset(string $scope): Configuration
    {
        if (null === $preset = $this->fromConfig($scope . '.preset')) {
            throw new InvalidArgumentException('Key "preset" is missing or empty');
        }

        if (!class_exists($preset) && !$this->container->bound($preset)) {
            throw new InvalidArgumentException('Class or service id for preset does not exists');
        }

        return $this->container->make($preset);
    }

    protected function resolveKey(string $scope): Key
    {
        if (null === $key = $this->fromConfig($scope . '.key')) {
            throw new InvalidArgumentException('Key "key" is missing or empty');
        }

        if (is_string($key)) {
            return new Key($key);
        }

        if (is_array($key)) {
            return new Key($key[0], $key[1]);
        }

        throw new InvalidArgumentException('"key" must be a string or array.');
    }

    protected function registerEventListener(string $scope): void
    {
        $eventListener = $this->fromConfig($scope . '.event_listener');

        if (null === $eventListener || $this->container->bound($eventListener)) {
            return;
        }

        $claims = $this->fromConfig($scope . '.claims');
        $validation = $this->fromConfig($scope . '.validation');

        if (null === $claims || null === $validation) {
            throw new InvalidArgumentException('Keys "validation" and "claims" are mandatories.');
        }

        $this->container->bind($eventListener, function (Application $app) use ($eventListener, $claims, $validation) {
            $listener = new $eventListener(
                $app->make($claims),
                $app->make($validation),
                $app->make('request')
            );

            return $listener;
        });

        $this->container['events']->subscribe($this->container->make($eventListener));
    }

    protected function fromConfig(string $key = null)
    {
        if (!$key) {
            return $this->config;
        }

        if (!isset($this->config[$this->namespace])) {
            throw new InvalidArgumentException(
                sprintf('Namespace %s does not exists in config.', $this->namespace));
        }

        return array_get($this->config[$this->namespace], $key);
    }
}