<?php

declare(strict_types=1);

namespace Hewsda\FirewallJwt\Exception;

class FirewallJwtAuthenticationException extends FirewallJwtException
{
}