<?php

declare(strict_types=1);

namespace Hewsda\FirewallJwt\Contracts;

use Illuminate\Http\Request;
use Lcobucci\JWT\Token;
use Lcobucci\JWT\ValidationData;
use Hewsda\Security\Foundation\Contracts\Core\User\LocalUser;

interface ClaimValidation
{
    public function __invoke(Token $token, LocalUser $account, Request $request): ValidationData;
}