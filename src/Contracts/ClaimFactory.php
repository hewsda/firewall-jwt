<?php

declare(strict_types=1);

namespace Hewsda\FirewallJwt\Contracts;

use Lcobucci\JWT\Builder;

interface ClaimFactory
{
    public function __invoke(Builder $builder): callable;
}