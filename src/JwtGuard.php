<?php

declare(strict_types=1);

namespace Hewsda\FirewallJwt;

use Hewsda\FirewallJwt\Event\JwtTokenWasDecoded;
use Hewsda\FirewallJwt\Event\JwtTokenWasEncoded;
use Hewsda\FirewallJwt\Exception\FirewallJwtAuthenticationException;
use Hewsda\FirewallJwt\Exception\FirewallJwtException;
use Hewsda\FirewallJwt\Preset\Configuration;
use Hewsda\Security\Foundation\Contracts\Core\Authentication\Token\Tokenable;
use Hewsda\Security\Foundation\Contracts\Core\User\LocalUser;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Http\Request;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Token;
use Lcobucci\JWT\ValidationData;

class JwtGuard
{
    /**
     * @var Configuration
     */
    private $configuration;

    /**
     * @var Key
     */
    private $key;

    /**
     * @var Dispatcher
     */
    private $events;

    /**
     * @var string
     */
    private $scope;

    /**
     * JwtGuard constructor.
     *
     * @param Configuration $configuration
     * @param Key $key
     * @param string $scope
     * @param Dispatcher $events
     */
    public function __construct(Configuration $configuration, Key $key, string $scope, Dispatcher $events)
    {
        $this->configuration = $configuration;
        $this->key = $key;
        $this->events = $events;
        $this->scope = $scope;
    }

    public function encode(Tokenable $token, Request $request): Token
    {
        $builder = $this->configuration->createBuilder();

        $builder = $this->fireEncodedEvent(new JwtTokenWasEncoded($builder, $token, $request));

        return $builder
            ->set('scope', $this->scope)
            ->setHeader('alg', $this->configuration->getSigner()->getAlgorithmId())
            ->sign($this->configuration->getSigner(), $this->key)
            ->getToken();
    }

    public function decode(Tokenable $jwtToken): Token
    {
        $token = $this->parseToken($jwtToken->credentials());

        $this->verifySignature($token);

        return $token;
    }

    final public function validate(Tokenable $jwtToken, LocalUser $user, Token $token): bool
    {
        $this->validateScope($token);

        $validator = $this->fireDecodedEvent(new JwtTokenWasDecoded($token, $user));

        if (!$token->validate($validator)) {
            throw new FirewallJwtAuthenticationException('Token validation failed.');
        }

        return true;
    }

    private function parseToken(string $token): Token
    {
        try {
            return $this->configuration->getParser()->parse($token);
        } catch (\InvalidArgumentException | \RuntimeException $parseException) {
            throw new FirewallJwtAuthenticationException('Jwt Token Parsing failed.', 0, $parseException);
        }
    }

    private function verifySignature(Token $token): void
    {
        try {
            if (!$token->verify($this->configuration->getSigner(), $this->key)) {
                throw new FirewallJwtAuthenticationException('Jwt Token Decoding failed.');
            }
        } catch (\RuntimeException $decodingException) {
            throw new FirewallJwtAuthenticationException('Jwt Token Decoding failed.', 0, $decodingException);
        }
    }

    private function validateScope(Token $token): void
    {
        if (!$token->hasClaim('scope') || $this->scope !== $token->getClaim('scope')) {
            throw new FirewallJwtAuthenticationException('Authentication denied. Wrong scope');
        }
    }

    private function fireEncodedEvent(JwtTokenWasEncoded $event): Builder
    {
        $payload = $this->events->dispatch($event);

        if (!$payload[0] instanceof Builder) {
            throw new FirewallJwtException('Return type from Jwt event must a Builder object.');
        }

        return $payload[0];
    }

    private function fireDecodedEvent(JwtTokenWasDecoded $event): ValidationData
    {
        $payload = $this->events->dispatch($event);

        if (!$payload[0] instanceof ValidationData) {
            throw new FirewallJwtException('Return type from Jwt event must a ValidationData object.');
        }

        return $payload[0];
    }
}