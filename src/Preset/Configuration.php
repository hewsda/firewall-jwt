<?php

declare(strict_types=1);

namespace Hewsda\FirewallJwt\Preset;

use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Claim\Factory;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Parsing\Decoder;
use Lcobucci\JWT\Parsing\Encoder;
use Lcobucci\JWT\Signer;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class Configuration
{
    /**
     * @var array
     */
    private $data;

    /**
     * Configuration constructor.
     */
    public function __construct()
    {
        $this->data = [];
    }

    public function createBuilder(): Builder
    {
        return new Builder($this->getEncoder(), $this->getClaimFactory());
    }

    public function getParser(): Parser
    {
        if (!array_key_exists('parser', $this->data)) {
            $this->data['parser'] = new Parser($this->getDecoder(), $this->getClaimFactory());
        }

        return $this->data['parser'];
    }

    public function setParser(Parser $parser): void
    {
        $this->data['parser'] = $parser;
    }

    public function getSigner(): Signer
    {
        if (!array_key_exists('signer', $this->data)) {
            $this->data['signer'] = new Sha256();
        }

        return $this->data['signer'];
    }

    public function setSigner(Signer $signer): void
    {
        $this->data['signer'] = $signer;
    }

    private function getClaimFactory(): Factory
    {
        if (!array_key_exists('claimFactory', $this->data)) {
            $this->data['claimFactory'] = new Factory();
        }
        return $this->data['claimFactory'];
    }

    public function setClaimFactory(Factory $claimFactory): void
    {
        $this->data['claimFactory'] = $claimFactory;
    }

    public function setEncoder(Encoder $encoder): void
    {
        $this->data['encoder'] = $encoder;
    }

    private function getEncoder(): Encoder
    {
        if (!array_key_exists('encoder', $this->data)) {
            $this->data['encoder'] = new Encoder();
        }

        return $this->data['encoder'];
    }

    public function setDecoder(Decoder $decoder): void
    {
        $this->data['decoder'] = $decoder;
    }

    private function getDecoder(): Decoder
    {
        if (!array_key_exists('decoder', $this->data)) {
            $this->data['decoder'] = new Decoder();
        }

        return $this->data['decoder'];
    }
}