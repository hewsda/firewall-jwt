<?php

declare(strict_types=1);

namespace Hewsda\FirewallJwt\Preset;

use Hewsda\FirewallJwt\Contracts\ClaimFactory;
use Hewsda\FirewallJwt\Contracts\ClaimValidation;
use Hewsda\FirewallJwt\Event\JwtTokenWasDecoded;
use Hewsda\FirewallJwt\Event\JwtTokenWasEncoded;
use Illuminate\Http\Request;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\ValidationData;
use Illuminate\Contracts\Events\Dispatcher;

class GenericAuthenticationEvent
{
    /**
     * @var ClaimFactory
     */
    private $factory;

    /**
     * @var ClaimValidation
     */
    private $validation;

    /**
     * @var Request
     */
    private $request;

    /**
     * GenericAuthenticationEvent constructor.
     *
     * @param ClaimFactory $factory
     * @param ClaimValidation $validation
     * @param Request $request
     */
    public function __construct(ClaimFactory $factory, ClaimValidation $validation, Request $request)
    {
        $this->factory = $factory;
        $this->validation = $validation;
        $this->request = $request;
    }

    public function onEncoded(JwtTokenWasEncoded $event): Builder
    {
        return ($this->factory)($event->builder())(
            $event->token()->user(), $event->request());
    }

    public function onDecoded(JwtTokenWasDecoded $event): ValidationData
    {
        return ($this->validation)($event->token(), $event->account(), $this->request);
    }

    public function subscribe(Dispatcher $events): void
    {
        $events->listen(JwtTokenWasEncoded::class,[$this,'onEncoded']);

        $events->listen(JwtTokenWasDecoded::class,[$this,'onDecoded']);
    }
}