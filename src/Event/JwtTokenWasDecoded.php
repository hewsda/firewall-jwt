<?php

declare(strict_types=1);

namespace Hewsda\FirewallJwt\Event;

use Hewsda\Security\Foundation\Contracts\Core\User\LocalUser;
use Lcobucci\JWT\Token;

class JwtTokenWasDecoded
{

    /**
     * @var Token
     */
    private $token;

    /**
     * @var LocalUser
     */
    private $account;

    /**
     * JwtTokenWasDecoded constructor.
     *
     * @param Token $token
     * @param LocalUser $account
     */
    public function __construct(Token $token, LocalUser $account)
    {
        $this->token = $token;
        $this->account = $account;
    }

    public function token(): Token
    {
        return $this->token;
    }

    public function account(): LocalUser
    {
        return $this->account;
    }
}