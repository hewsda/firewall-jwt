<?php

declare(strict_types=1);

namespace Hewsda\FirewallJwt\Event;

use Hewsda\Security\Foundation\Contracts\Core\Authentication\Token\Tokenable;
use Illuminate\Http\Request;
use Lcobucci\JWT\Builder;

class JwtTokenWasEncoded
{
    /**
     * @var Builder
     */
    private $builder;

    /**
     * @var Tokenable
     */
    private $token;

    /**
     * @var Request
     */
    private $request;

    /**
     * JwtTokenWasEncoded constructor.
     *
     * @param Builder $builder
     * @param Tokenable $token
     * @param Request $request
     */
    public function __construct(Builder $builder, Tokenable $token, Request $request)
    {
        $this->builder = $builder;
        $this->token = $token;
        $this->request = $request;
    }

    public function builder(): Builder
    {
        return $this->builder;
    }

    public function token(): Tokenable
    {
        return $this->token;
    }

    public function request(): Request
    {
        return $this->request;
    }
}